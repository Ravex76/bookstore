﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WCFHost
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceHost host = new ServiceHost(typeof(WCFBookService.BookGetData));
            try
            {
                host.Open();
                Console.WriteLine("Нажмите ENTER для закрытия сервиса...");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Ошибка запуска WCF-сервиса. Детальная информация: {ex.Message}");
                
            }            
            Console.ReadLine();
            if (host.State == CommunicationState.Opened)
                host.Close();
        }
    }
}
