﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml.Linq;

namespace WCFBookService
{
    public class BookGetData : IBookGetData
    {
        public BookData GetData()
        {
            BookData bookList = new BookData();
            XDocument xDoc;

            try
            {
                xDoc = XDocument.Load("books.xml");
            }
            catch (Exception ex)
            {
                throw new Exception($"Ошибка при открытии файла 'books.xml'. Детальная информация: {ex.Message}");
            }

            var root = xDoc.Element("Books");
            if (root == null)
                throw new Exception("Отсутствует корневой элемент в 'books.xml'.");

            foreach (var bookElement in root.Elements("Book"))
            {
                Book book = new Book
                {
                    Title = bookElement.Element("Title").Value,
                    Author = bookElement.Element("Author").Value,
                    Description = bookElement.Element("Description").Value,
                    Year = Int32.TryParse(bookElement.Element("Year").Value, out int year) ? year : 0,
                    Url = bookElement.Element("Url").Value,
                    CatalogSection = bookElement.Element("CatalogSection").Value,
                    Image = GetImage(bookElement.Element("CoverPath").Value)
                };

                bookList.Add(book);
            }

            return bookList;
        }

        /// <summary>
        /// Метод считывает картинку из файла в массив байтов
        /// </summary>
        /// <param name="_value">Относительный путь к файлу</param>
        /// <returns></returns>
        private byte[] GetImage(string _value)
        {
            byte[] result = null;
            try
            {
                result = File.ReadAllBytes(System.Environment.CurrentDirectory + _value);
            }
            catch 
            {
            }

            return result;
        }
    }
}
