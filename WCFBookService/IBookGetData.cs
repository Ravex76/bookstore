﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCFBookService
{
    [ServiceContract]
    public interface IBookGetData
    {
        /// <summary>
        /// Метод возвращает список книг
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        BookData GetData();
    }
}
