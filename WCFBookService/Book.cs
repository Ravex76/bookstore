﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WCFBookService
{
    /// <summary>
    /// Класс описывает экземпляр книги
    /// </summary>
    [DataContract]
    public class Book
    {
        [DataMember]
        public string Title;

        [DataMember]
        public string Author;

        [DataMember]
        public string Description;

        [DataMember]
        public int Year;

        [DataMember]
        public string Url;

        [DataMember]
        public string CatalogSection;

        [DataMember]
        public byte[] Image;
    }

    /// <summary>
    /// Класс описывает список книг
    /// </summary>
    [CollectionDataContract]
    public class BookData : List<Book>
    {

    }
}
