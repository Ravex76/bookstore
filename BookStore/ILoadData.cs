﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore
{
    /// <summary>
    /// Интерфейс описывает функционал загрузки списка книг
    /// </summary>
    public interface ILoadData
    {
        /// <summary>
        /// Метод возвращает список книг
        /// </summary>
        /// <returns></returns>
        List<Book> LoadData();
    }
}
