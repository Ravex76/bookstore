﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore
{
    /// <summary>
    /// Класс описывает категорию книг
    /// </summary>
    public class Category
    {
        /// <summary>
        /// Название категории
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Количество книг в категории
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Заголовок категории для элемента списка 
        /// </summary>
        public string Caption => $"{Name} ({Count})";
    }
}
