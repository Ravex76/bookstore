﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace BookStore
{
    /// <summary>
    /// Класс описывает экземпляр книги
    /// </summary>
    public class Book: INotifyPropertyChanged
    {
        private string title;
        private string author;
        private string description;
        private int year;
        private string url;
        private string catalogSection;
        private BitmapImage cover;

        public Book()
        {

        }

        /// <summary>
        /// Название книги
        /// </summary>
        public string Title
        {
            get => title;
            set
            {
                title = value;
                OnPropertyChanged(nameof(Title));
            }
        }

        /// <summary>
        /// Автор
        /// </summary>
        public string Author
        {
            get => author;
            set
            {
                author = value;
                OnPropertyChanged(nameof(Author));
            }
        }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description
        {
            get => description;
            set
            {
                description = !String.IsNullOrEmpty(value) ? value : "[ Описание отсутствует ]";
                OnPropertyChanged(nameof(Description));
            }
        }

        /// <summary>
        /// Год выпуска
        /// </summary>
        public int Year
        {
            get => year;
            set
            {
                year = value;
                OnPropertyChanged(nameof(Year));
            }
        }

        /// <summary>
        /// Ссылка на электронную версию
        /// </summary>
        public string Url
        {
            get => url;
            set
            {
                url = value;
                OnPropertyChanged(nameof(Url));
            }
        }

        /// <summary>
        /// Раздел каталога
        /// </summary>
        public string CatalogSection
        {
            get => catalogSection;
            set
            {
                catalogSection = value;
                OnPropertyChanged(nameof(CatalogSection));
            }
        }

        /// <summary>
        /// Обложка
        /// </summary>
        public BitmapImage Cover
        {
            get => cover;
            set
            {
                cover = value;
                OnPropertyChanged(nameof(Cover));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName]string _property = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(_property));
        }
    }
}
