﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BookStore
{
    /// <summary>
    /// Класс главного окна приложения
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            // в зависимости от настроек список книг будет загружаться или с помощью WCF-сервиса,
            // или из xml-файла
            ILoadData loadData = Properties.Settings.Default.WCFLoad ? (ILoadData)new WCFLoadData() : 
                                                                       new FilesLoadData();
            DataContext = new MainViewModel(loadData);
        }

        private void stageBtn_Checked(object sender, RoutedEventArgs e)
        {
            if (sender is RadioButton rb)
                SetBookListView((bool)rb.IsChecked);
        }

        /// <summary>
        /// Устанавливает вид отображения списка книг
        /// </summary>
        /// <param name="_isStageView">True - витрина, False - список</param>
        private void SetBookListView(bool _isStageView)
        {
            if (_isStageView)
            {
                bookListBox.ItemTemplate = (DataTemplate)Resources["stageTemplate"];
                bookListBox.ItemsPanel = (ItemsPanelTemplate)Resources["wrapPanelTemplate"];
            }
            else
            {
                bookListBox.ItemTemplate = (DataTemplate)Resources["listTemplate"];
                bookListBox.ItemsPanel = (ItemsPanelTemplate)Resources["stackPanelTemplate"];
            }
        }
    }
}
