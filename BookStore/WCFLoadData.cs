﻿using BookStore.WCFBookService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace BookStore
{
    /// <summary>
    /// Класс реализует функционал загрузки списка книг посредством WCF-сервиса
    /// </summary>
    public class WCFLoadData : ILoadData
    {
        public List<Book> LoadData()
        {
            BookGetDataClient client;
            BookData loadBooks;
            List<Book> bookList = new List<Book>();

            try
            {
                client = new BookGetDataClient("BasicHttpBinding_IBookGetData");
                loadBooks = client.GetData();
            }
            catch (Exception ex)
            {
                throw new Exception("Ошибка при загрузке списка книг с помощью WCF-сервиса.", ex);
            }

            foreach (var loadBook in loadBooks)
            {
                bookList.Add(new Book
                {
                    Title = loadBook.Title,
                    Author = loadBook.Author,
                    Description = loadBook.Description,
                    Year = loadBook.Year,
                    Url = loadBook.Url,
                    CatalogSection = loadBook.CatalogSection,
                    Cover = GetImage(loadBook.Image)
                });
            }

            bookList.Sort((x, y) => x.Title.CompareTo(y.Title));
            return bookList;
        }

        /// <summary>
        /// Метод преобразует массив байт в BitmapImage
        /// </summary>
        /// <param name="_bytes">Массив байт</param>
        /// <returns></returns>
        private BitmapImage GetImage(byte[] _bytes)
        {
            if (_bytes == null)
                return null;
            try
            {
                MemoryStream stream = new MemoryStream(_bytes);
                BitmapImage image = new BitmapImage();
                image.BeginInit();
                image.StreamSource = stream;
                image.EndInit();
                return image;
            }
            catch 
            {
                return null;
            }
        }
    }
}
