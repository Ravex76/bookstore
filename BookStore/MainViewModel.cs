﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace BookStore
{
    /// <summary>
    /// Класс описывает модель представления
    /// </summary>
    public class MainViewModel : INotifyPropertyChanged
    {
        // В данном поле хранится полный нефильтрованный список книг
        private List<Book> originalBookList;

        /// <summary>
        /// Список категорий
        /// </summary>
        public List<Category> CategoryData { get; set; }

        /// <summary>
        /// Отображаемый список книг
        /// </summary>
        private ObservableCollection<Book> bookData;      
        public ObservableCollection<Book> BookData
        {
            get => bookData;
            set
            {
                bookData = value;
                OnPropertyChanged(nameof(BookData));
            }
        }

        /// <summary>
        /// Выбранная категория
        /// </summary>
        private Category selectedCategory;
        public Category SelectedCategory
        {
            get => selectedCategory;
            set
            {
                selectedCategory = value;
                BookData = new ObservableCollection<Book>(originalBookList.Where(x => x.CatalogSection == selectedCategory.Name));
                OnPropertyChanged(nameof(SelectedCategory));
            }             
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="_loadBehavior">Экземпляр класса, реализующий интерфейс ILoadData</param>
        public MainViewModel(ILoadData _loadBehavior)
        {
            try
            {
                originalBookList = _loadBehavior.LoadData();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"{ex.Message} Детальная информация: {ex.InnerException.Message}");
                return;
            }

            bookData = new ObservableCollection<Book>(originalBookList);
            // список категорий формируется непосредственно из тех значений,
            // что были указаны в соотв. полях книг
            CategoryData = originalBookList.GroupBy(x => x.CatalogSection)
                            .Select(x => new Category
                                         {
                                            Name = x.Key,
                                            Count = x.Count()
                                         }).ToList();
        }

        /// <summary>
        /// Команда открытия ссылки
        /// </summary>
        private DelegateCommand openUrlCommand;
        public DelegateCommand OpenUrlCommand
        {
            get
            {
                return openUrlCommand ??
                    (openUrlCommand = new DelegateCommand(obj =>
                    {
                        if (obj is string url)
                            try
                            {
                                System.Diagnostics.Process.Start(url);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Ошибка при открытии сетевого ресурса: " + ex.Message);
                            }
                            
                    }));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName]string _property = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(_property));
        }
    }
}
