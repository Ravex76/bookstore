﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace BookStore
{
    /// <summary>
    /// Класс реализует функционал загрузки списка книг из xml-файла
    /// </summary>
    public class FilesLoadData : ILoadData
    {
        // Путь к месторасположению xml-файла со списком книг и каталогом с картинками.        
        private readonly string filePath = Properties.Settings.Default.FilePath;

        public List<Book> LoadData()
        {
            List<Book> bookList = new List<Book>();
            XDocument xDoc;

            try
            {
                xDoc = XDocument.Load(filePath + "books.xml");
            }
            catch (Exception ex)
            {
                throw new Exception("Ошибка при открытии файла 'books.xml'.", ex);
            }

            var root = xDoc.Element("Books");
            if (root == null)
                throw new Exception("Отсутствует корневой элемент в 'books.xml'.");

            foreach (var bookElement in root.Elements("Book"))
            {
                Book book = new Book
                {
                    Title = bookElement.Element("Title").Value,
                    Author = bookElement.Element("Author").Value,
                    Description = bookElement.Element("Description").Value,
                    Year = Int32.TryParse(bookElement.Element("Year").Value, out int year) ? year : 0,
                    Url = bookElement.Element("Url").Value,
                    CatalogSection = bookElement.Element("CatalogSection").Value,
                    Cover = GetImage(bookElement.Element("CoverPath").Value)
                };

                bookList.Add(book);
            }

            bookList.Sort((x, y) => x.Title.CompareTo(y.Title));
            return bookList;
        }

        /// <summary>
        /// Метод получения BitmapImage по относительному пути к файлу с картинкой
        /// </summary>
        /// <param name="_coverPath">Отностительный путь к местоположению картинки</param>
        /// <returns></returns>
        private BitmapImage GetImage(string _coverPath)
        {
            if (String.IsNullOrWhiteSpace(_coverPath))
                return null;
            
            BitmapImage bmpImage = new BitmapImage();
            try
            {
                bmpImage.BeginInit();
                bmpImage.UriSource = new Uri((!String.IsNullOrEmpty(filePath) ? filePath : AppDomain.CurrentDomain.BaseDirectory) + _coverPath);
                bmpImage.CacheOption = BitmapCacheOption.OnLoad;
                bmpImage.EndInit();

            }
            catch 
            {
                bmpImage = null;
            }

            return bmpImage;
        }
    }
}
